/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dalimao.library.util;

/**
 * window显示的层级
 *
 * @since 2021-06-30
 */
public enum TypeFlag {
    /**
     * 应用内窗体
     */
    MOD_APPLICATION_MEDIA,
    /**
     * 应用外窗体
     */
    MOD_APP_OVERLAY
}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dalimao.library.util;

import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * BignessUtil
 *
 * @since 2021-06-25
 */
public class BignessUtil {
    /**
     * componentList
     */
    private static List<WindowInfo> componentList = new ArrayList<>();

    private BignessUtil() {
    }

    public static List<WindowInfo> getComponentList() {
        return componentList;
    }

    /**
     * setComponentList
     *
     * @param component component
     * @return List
     */
    public static List<WindowInfo> setComponentList(WindowInfo component) {
        componentList.add(component);
        return componentList;
    }

    /**
     * 获取屏幕密度
     *
     * @param context context
     * @return DisplayAttributes
     */
    public static DisplayAttributes getScreenPiex(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = null;
        if (display.isPresent()) {
            displayAttributes = display.get().getAttributes();
        }
        return displayAttributes;
    }

    /**
     * 获取屏幕高度，不包含状态栏的高度
     *
     * @param context 上下文
     * @return 屏幕高度，不包含状态栏的高度
     */
    public static float getDisplayHeightInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getSize(point);
        return point.getPointY();
    }

    /**
     * getStatusBar
     *
     * @param context 上下文
     * @return float
     */
    public static float getStatusBar(Context context) {
        float barHeight = (float) NumberUtil.sub(getScreenPiex(context).height,getDisplayHeightInPx(context));
        return barHeight;
    }

    /**
     * 获取状态栏高度
     *
     * @param context 上下文
     * @return int
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        Point point1 = new Point();
        Point point2 = new Point();
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        display.getSize(point1);
        display.getRealSize(point2);
        result = (int) NumberUtil.sub(point2.getPointY(),point1.getPointY());
        return result;
    }

    /**
     * 保存所有窗口view的实例 同一个 View 类，同时只能显示一个实例
     *
     * @param componentName 类名称
     * @return List
     */
    public static List<WindowInfo> setInstanceComponent(WindowInfo componentName) {
        return setComponentList(componentName);
    }

    /**
     * 当前实例 是否存在
     *
     * @param componentName 类名
     * @return boolean
     */
    public static WindowInfo isExistComponent(WindowInfo componentName) {
        WindowInfo info = null;
        for (int index = 0; index < getComponentList().size(); index++) {
            WindowInfo mComponent = getComponentList().get(index);
            if (mComponent.getClassName().equals(componentName.getClassName())) {
                info = mComponent;
                break;
            }
        }
        return info;
    }

    /**
     * 删除实例
     *
     * @param componentName 类名
     */
    public static void deleteExistComponent(WindowInfo componentName) {
        for (int index = 0; index < getComponentList().size(); index++) {
            WindowInfo mComponent = getComponentList().get(index);
            if (mComponent.getClassName().equals(componentName.getClassName())) {
                getComponentList().remove(index);
                break;
            }
        }
    }
}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dalimao.floateutil;

import com.dalimao.library.FloatUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * 单元测试
 *
 * @since 2021-07-07
 */
public class ExampleOhosTest {
    private Context mContext = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();

    /**
     * 包名测试
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.dalimao.floateutil", actualBundleName);
    }

    /**
     * FloatBallView测试
     */
    @Test
    public void testFloatBallView() {
        FloatBallView floatBallView = new FloatBallView(mContext);
        assertNotNull(floatBallView);
    }

    /**
     * SimpleView测试
     */
    @Test
    public void testSimpleView() {
        SimpleView simpleView = new SimpleView(mContext);
        assertNotNull(simpleView);
    }

    /**
     * SimpleViewWitchParam测试
     */
    @Test
    public void testSimpleViewWitchParam() {
        SimpleViewWitchParam simpleViewWitchParam = new SimpleViewWitchParam(mContext);
        assertNotNull(simpleViewWitchParam);
    }

    /**
     * FloatUtil测试
     */
    @Test
    public void testFloatUtil() {
        SimpleView simpleView = new SimpleView(mContext);
        FloatUtil builder = new FloatUtil.Builder().setComponent(simpleView).showFloatView(mContext);
        assertNotNull(builder);
    }
}
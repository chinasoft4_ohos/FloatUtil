package com.dalimao.floateutil;

import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * SimpleViewWitchParam
 *
 * @since 2021-06-29
 */
public class SimpleViewWitchParam extends StackLayout {
    /**
     * PARAM
     */
    public static final String PARAM = "PARAM";
    /**
     * CONTENT
     */
    public static final String CONTENT = "content";
    Component rootView;

    /**
     * SimpleViewWitchParam
     *
     * @param context
     */
    public SimpleViewWitchParam(Context context) {
        super(context);
        rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_float_simple_witch_param, this, true);
    }

    /**
     * onParamReceive
     *
     * @param args
     */
    public void onParamReceive(IntentParams args) {
        if (args != null) {
            String param = (String) args.getParam(PARAM);
            Text textView = (Text) rootView.findComponentById(ResourceTable.Id_tv_param);
            textView.setText(param);
            String content = (String) args.getParam(CONTENT);
            if (content != null && content.length() > 0) {
                Text tvContent = (Text) rootView.findComponentById(ResourceTable.Id_tv_content);
                tvContent.setText(content);
            }
        }
    }
}

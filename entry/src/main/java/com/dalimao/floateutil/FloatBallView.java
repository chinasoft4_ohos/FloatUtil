package com.dalimao.floateutil;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * FloatBallView
 *
 * @since 2021-07-01
 */
public class FloatBallView extends StackLayout {
    private static final float RADIUSNUM = 100f;

    /**
     * FloatBallView
     *
     * @param context
     */
    public FloatBallView(Context context) {
        super(context);
        Component rootView = LayoutScatter.getInstance(context).
                parse(ResourceTable.Layout_float_small_drag_ball,
                        this, true);
        Image imageView = (Image) rootView.findComponentById(ResourceTable.Id_imageView);
        imageView.setCornerRadius(RADIUSNUM);
    }
}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dalimao.floateutil.slice;

import com.dalimao.library.FloatUtil;
import com.dalimao.floateutil.ResourceTable;
import com.dalimao.floateutil.SimpleView;
import com.dalimao.floateutil.FloatBallView;
import com.dalimao.floateutil.SimpleViewWitchParam;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.sysappcomponents.settings.AppSettings;
import ohos.utils.net.Uri;

/**
 * SimpleViewWitchParam
 *
 * @since 2021-06-29
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final int FLOATBALLVIEWSIZE = 45;
    private static final int POINTX = 100;
    private static final int POINTY = 300;
    private static final int REQUESTCODE = 1000;
    private static final String PARAMVALUE = "我是传过来的参数";
    Button mAddSimpleView;
    Button mAddSimpleViewWithParam;
    Button mAddSimpleViewWithGravity;
    Button mAddSimpleViewWithType;
    Button mAddSimpleViewWithPoint;
    Button mAddSimpleSmartFloatView;
    Button mAddDragView;
    IntentParams params;
    SimpleViewWitchParam smartFloatView;
    Point point;
    FloatUtil builder;

    /**
     * onStart
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mAddSimpleView = (Button) findComponentById(ResourceTable.Id_addSimpleView);
        mAddSimpleViewWithParam = (Button) findComponentById(ResourceTable.Id_addSimpleViewWithParam);
        mAddSimpleViewWithGravity = (Button) findComponentById(ResourceTable.Id_addSimpleViewWithGravity);
        mAddSimpleViewWithType = (Button) findComponentById(ResourceTable.Id_addSimpleViewWithType);
        mAddSimpleViewWithPoint = (Button) findComponentById(ResourceTable.Id_addSimpleViewWithPoint);
        mAddSimpleSmartFloatView = (Button) findComponentById(ResourceTable.Id_addSimpleSmartFloatView);
        mAddDragView = (Button) findComponentById(ResourceTable.Id_addDragView);
        mAddSimpleView.setClickedListener(this);
        mAddSimpleViewWithParam.setClickedListener(this);
        mAddSimpleViewWithGravity.setClickedListener(this);
        mAddSimpleViewWithType.setClickedListener(this);
        mAddSimpleViewWithPoint.setClickedListener(this);
        mAddSimpleSmartFloatView.setClickedListener(this);
        mAddDragView.setClickedListener(this);
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * onForeground
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * onClick
     *
     * @param component component
     */
    @Override
    public void onClick(Component component) {
        params = new IntentParams();
        smartFloatView = new SimpleViewWitchParam(this);
        point = new Point();
        boolean isAuthority = AppSettings.canShowFloating(this);
        if (!isAuthority) {
            Intent intent = new Intent();
            intent.setAction("android.settings.action.MANAGE_OVERLAY_PERMISSION");
            Uri parse = Uri.parse("package:" + getBundleName());
            intent.setUri(parse);
            startAbilityForResult(intent, REQUESTCODE);
            return;
        }
        switch (component.getId()) {
            case ResourceTable.Id_addSimpleView:
                setIdAddSimpleView();
                break;

            case ResourceTable.Id_addSimpleViewWithParam:
                setIdAddSimpleViewWithParam();
                break;

            case ResourceTable.Id_addSimpleViewWithGravity:
                setIdAddSimpleViewWithGravity();
                break;

            case ResourceTable.Id_addSimpleViewWithType:
                setIdAddSimpleViewWithType();
                break;

            case ResourceTable.Id_addSimpleViewWithPoint:
                setIdAddSimpleViewWithPoint();
                break;

            case ResourceTable.Id_addSimpleSmartFloatView:
                setIdAddSimpleSmartFloatView();
                break;

            case ResourceTable.Id_addDragView:
                setIdAddDragView();
                break;
            default:
                break;
        }
    }

    private void setIdAddSimpleView() {
        SimpleView floatView = new SimpleView(this);

        builder = new FloatUtil.Builder()
                .setComponent(floatView)
                .showFloatView(this);
        floatView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(floatView);
                });
    }

    private void setIdAddSimpleViewWithParam() {
        params.setParam(SimpleViewWitchParam.PARAM, PARAMVALUE);
        smartFloatView.onParamReceive(params);
        builder = new FloatUtil.Builder()
                .setComponent(smartFloatView)
                .showFloatView(this);
        smartFloatView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(smartFloatView);
                });
    }

    private void setIdAddSimpleViewWithGravity() {
        params.setParam(SimpleViewWitchParam.PARAM, PARAMVALUE);
        params.setParam(SimpleViewWitchParam.CONTENT, getString(ResourceTable.String_add_simple_view_with_gravity));
        smartFloatView.onParamReceive(params);
        builder = new FloatUtil.Builder()
                .setComponent(smartFloatView)
                .setGravity(LayoutAlignment.CENTER)
                .showFloatView(this);
        smartFloatView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(smartFloatView);
                });
    }

    private void setIdAddSimpleViewWithType() {
        params.setParam(SimpleViewWitchParam.PARAM, PARAMVALUE);
        params.setParam(SimpleViewWitchParam.CONTENT, getString(ResourceTable.String_add_simple_view_with_type));
        smartFloatView.onParamReceive(params);
        builder = new FloatUtil.Builder()
                .setComponent(smartFloatView)
                .setGravity(LayoutAlignment.CENTER)
                .showFloatView(this);
        smartFloatView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(smartFloatView);
                });
    }

    private void setIdAddSimpleViewWithPoint() {
        params.setParam(SimpleViewWitchParam.PARAM, PARAMVALUE);
        params.setParam(SimpleViewWitchParam.CONTENT, getString(ResourceTable.String_add_simple_view_with_point));
        point = new Point(POINTX, POINTY);

        smartFloatView.onParamReceive(params);
        builder = new FloatUtil.Builder()
                .setComponent(smartFloatView)
                .setGravity(LayoutAlignment.TOP)
                .point(point)
                .showFloatView(this);
        smartFloatView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(smartFloatView);
                });
    }

    private void setIdAddSimpleSmartFloatView() {
        params.setParam(SimpleViewWitchParam.PARAM, "智能浮窗");
        params.setParam(SimpleViewWitchParam.CONTENT, getString(ResourceTable.String_add_simple_view_with_smart));
        smartFloatView.onParamReceive(params);

        builder = new FloatUtil.Builder()
                .setComponent(smartFloatView)
                .setGravity(LayoutAlignment.CENTER)
                .point(point)
                .showFloatView(this);
        smartFloatView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(smartFloatView);
                });
    }

    private void setIdAddDragView() {
        SimpleViewWitchParam simpleView = new SimpleViewWitchParam(this);
        FloatBallView floatBallView = new FloatBallView(this);

        new FloatUtil.Builder()
                .setComponent(floatBallView)
                .setWidth(FLOATBALLVIEWSIZE)
                .setHeight(FLOATBALLVIEWSIZE)
                .setIsMovable(false)
                .showFloatView(this);
        builder = new FloatUtil.Builder()
                .setComponent(simpleView)
                .setIsMovable(true)
                .showFloatView(this);

        simpleView.findComponentById(ResourceTable.Id_close)
                .setClickedListener(view -> {
                    builder.hideFloatView(simpleView);
                });
    }
}
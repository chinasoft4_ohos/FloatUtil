package com.dalimao.floateutil;

import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * SimpleView
 *
 * @since 2021-06-28
 */
public class SimpleView extends StackLayout {
    /**
     * SimpleView
     *
     * @param context
     */
    public SimpleView(Context context) {
        super(context);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_float_simple,
                this, true);
    }
}

# FloatUtil

#### 项目介绍
- 项目名称：FloatUtil
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个简单的浮窗工具。封装了浮窗的使用方法。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.0

#### 效果演示

<img src="gif/videoView.gif"></img>

#### 安装教程


1.在项目根目录下的build.gradle文件中，
```java
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
```java
 dependencies {
    implementation('com.gitee.chinasoft_ohos:FloatUtil:1.0.0')
    ......
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.初始化:

```java
   SimpleView floatView = new SimpleView(this);
   FloatUtil builder = new FloatUtil.Builder()
        .Component(floatView)
        .build(this);
```
SimpleView 是你自定义的 Component，就这么简单，浮窗显示出来了

2.关闭浮窗
```java
builder.hideFloatView(floatView);
```
同一个 Component 类，同时只能显示一个实例， 关闭的时候指定一个 Component 对象便能知道关闭哪个浮窗实例

3.向浮窗传递参数，在当前窗口的自定义Component定义函数onParamReceive(IntentParams args)通过
IntentParams args进行传递参数
```java
IntentParams params  = new IntentParams();
floatView.onParamReceive(params);
```
4.指定层级和对其方式
```java
new FloatUtil.Builder()
        .setComponent(smartFloatView) //window 窗口布局
        .setGravity(LayoutAlignment.CENTER) //对其方式
        .setTypeFlag(TypeFlag.MOD_APPLICATION_MEDIA) //指定层级
        .build(this);
        
//应用内窗体 TypeFlag.MOD_APPLICATION_MEDIA
//应用外窗体 TypeFlag.MOD_APP_OVERLAY
```

5.接口说明:

```java
/**
 * view 自定义布局 
 */
setComponent(Component view)

/**
 * gravity 对其方式
 */
setGravity(int gravity)

/**
 * height 自定义窗体高度
 */
setHeight(int height)

/**
 * width 自定义窗体高度
 */
setWidth(int width)

/**
 * isMovable 是否可以拖动
 */
setIsMovable(boolean isMovable)

/**
 * typeFlag 窗口层级
 */
setTypeFlag(TypeFlag typeFlag)

/**
 * point 指定x，y位置
 */
point(Point point)
```

## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代

- 1.0.0

## 版权和许可信息
```
MIT License

Copyright (c) 2016 刘光利

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```